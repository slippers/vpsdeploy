# vpsdeploy

configure remote linux server to run python webapp.

server requires debian server running ssh, nginx, postgres, uwsgi

## install

    python3 -m venv venv
    source venv/bin/activate
    pip --extra-index-url https://gitlab.com/api/v4/projects/26610248/packages/pypi/simple install vpsdeploy

### vps_deploy cli tool

use the cli tool provided by vpsdeploy package to get your project started.

    Usage: vps_deploy [OPTIONS] COMMAND [ARGS]...
    
    Options:
      --help  Show this message and exit.
    
    Commands:
     config-build    generate the default fabric.yaml config file.
     config-display  show your current fabric.yaml config
     invoke-patch    patch the invoke.loader.py file to include the system...
     invoke-unpatch  restore the patched invoke.loader.py file
     list            list all the vpsdeploy commands.

### patch invoke.loader.py

use the `vpsdeploy` cli tool to update the invoke.loader.py file

   vps_deploy invoke_patch

### list vpsdeploy tasks 

two ways to check if fabric can see the vpsdeploy tasks.
should see a listing of fabric tasks.

    vps_deploy list

or

    fab -c vpsdeploy -l

Available tasks:


    celery.setup            setup celery
    deploy.refresh          refresh the web app files from git repo
    deploy.server-setup     call server.upgrade and server.install
    deploy.web-setup        setup web app on server
    repo.create             create /var/repo on remote server and apply user permissions
    repo.deploy             clone or fetch the git repo on remote host
    repo.push               sync before push
    repo.sync               create repo at /var/repo if not exist
    server.install          apt-get install nginx and uwsig packages decalared in config.install
    server.uname            uname -a
    server.update           apt-get update
    server.upgrade          apt-get upgrade
    service.enable          execute the make enable on the remote host
    service.setup           send the files to the remote host webapp
    service.status          execute the make status on the remote host
    settings.context        report on invoke context configuration
    settings.default        print the local project fabric config settings
    ssl.http-validation     construct a nginx web directory. configure a nginx.conf file
    ssl.upload-validation   upload all txt files found in http_validation directory to the remote web directory
    venv.create             create and activate python3 virtualenv
    venv.install            install python requrirments using `pip install`
    www.user                create system linux user account
    www.webapp              create webapp directory on remote host


### test fabric ssh connection

use the server.uname task to get the remote server uname.
connecting to remote server is done via ssh.

    fab -H lof -c vpsdeploy server.uname

be sure to use ssh-add to add your host identity and 
an alias via your ~/.ssh/config.

### setup remote server admin user authentication

the remote host authenticated user needs sudo.
store the remote host password in your gnu password util.

     vps_deploy set-host-key


### fabric and invoke

now your going to need a fabric.yaml file with the vpsdeploy
configuration.

    vps_deploy config-build

the config can be cut down to only the parts you want to override.

    nginx:
      app: /puddle/main
      locations:
        - main/static
        - main/media
    uwsgi:
      application: main.wsgi:application
    venv:
      requirements: requirements/base.txt
    www:
      project: puddle


### deploy.web-setup and deploy.refresh

at this point you should be able to
* connect to remote host
* authenticate the remote host user with sudo
* have the server configured for web hosting
* the python webapp working local on dev machine

    fab -H host -f fabric.yaml -c vpsdeploy deploy.web-setup

**deploy.web-setup**
* setup repo on server
* push local git to server repo
* create webapp under /var/www
* clone or checkout from server repo
* configure the virtualenv
* install pip requirements
* service (uwsgi, nginx, systemd) configuration

after you webapp has been deployed and configured
you can refresh your webapp as needed

    fab -H host -f fabric.yaml -c vpsdeploy deploy.refresh

**deploy.refresh**
* push local git to server repo
* checkout from server repo
* install pip requirements

## the other vpsdeploy tasks

### git

you can use git to sync changes between machines. the basic
flow will be to create a local git repo with your files, generate
a repo on the remote webserver and sync changes to that.

**repo.setup**

create a repo directory on the remote machine.
requires sudo to create the directoy on /var/repo

**repo.sync**

local command to sync to remote server.
this does not move files to your webapp on remote server.

**repo.deploy**

clone or fetch repo into webapp directory on remote server.


### python venv

given a webapp project directory

**venv.create**

create the virtual environment on remote server

**venv.install**

install the requirements into the virtualenv


### webapp configuration

once all the files are in place the final step of hosting 
the app can begin. there are three systems to integrate.

These three systems will be used to make the webapp live.

**uwsgi, nginx, systemd**

uwsgi serves up the webapp.  a hightly configurable server.

nginx is the http server that will use uwsgi

systemd will handle the sockets and restarting of the uwsgi server.


### uwsgi

we will use socket activation to bind the nginx http server to the uwsgi server.

when a request comes in from the nginx it will be routed to the socket
connected to you uwsgi that points to your webapp.

when the socket is activated it will create a uwsgi service on demand.

you can stop, stop and get status via systemd.


### makefile and configs.

the three systems are represented as configurations that need to be
implemented and installed.

**service.preview** 

you can preview the configuations without installing anything

**service.local**

create a local set of config files in uwsgi directory

**service.send**

creates a uwsgi dir in your webapp with the config files.

**service.status**

this is a sudo command
calls the Makefile using `make status`


### make

make needs to be run with sudo.

in order to isolate your webapp activity from another we will create a linux
user www-{webapp} named the same as your webapp name.  this user will
get referenced in directory permissions, socket.

all - config info
status - socket and service status
install - install the serivces, install packages, create user
uninstall - remove the systemd parts
enable - enable calls install and enables the systemd parts
disable - stops the systemd parts
nginx - copies the nginx config and enables the site.

use:
    make enable
    make nginx


## Integration Levels

low integration security will tend to use
the default www-data user and temp directory. 
this creates a low security situation where
if there are multiple apps then they will
be able to see eachother.

high integration security will use a specific
user in protected directories.  process will
have to be started with the user context.

### user 

the default user and group is www-data 
as a built in security context.

to separate distinct application spaces
there is the option of generate a unique
users that will belong to www-data group

the unique user belongs to the application space
so once the application name is determined the user
is determined.

    applicaiton = special
    user = www-special

then it is a matter of creating the user and adding
it to the www-data group

these users have no special access to logon or home
folder stuff.  
