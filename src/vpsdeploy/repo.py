from pathlib import Path
from fabric import task
from patchwork.files import directory, exists
from .util import module_namespace
from .keyring import sudo_password


def repo_path(ctx):
    '''get the name of the git repository
    '''
    repo_path = ctx.local('git rev-parse --show-toplevel')
    repo_name = Path(repo_path.stdout.strip()).stem
    repo = Path(ctx.config.repo.root, repo_name + '.git').resolve()
    return repo


@task
def git_init(ctx):
    '''create /var/repo on remote server and apply user permissions
    needs sudo
    uses:
        config.repo.root = default to /var/repo
        config.repo.user = linux user
        config.repo.group = linux group
    '''
    sudo_password(ctx)
    repo = repo_path(ctx)
    user = ctx.config.repo.user if ctx.config.repo.user is not None else ctx.user
    group = ctx.config.repo.group if ctx.config.repo.group is not None else None
    # create /var/repo directory
    if not exists(ctx, ctx.config.repo.root):
        # requires sudo password
        directory(ctx, ctx.config.repo.root,
                  user=user,
                  group=group,
                  mode=ctx.config.repo.mode,
                  sudo=True)

    # build a new server git repo
    if not exists(ctx, repo):
        directory(ctx, repo)

    with ctx.cd(str(repo)):
        ctx.run('git --bare init')


@task
def git_sync(ctx):
    '''create repo at /var/repo if not exist

    link local git repository to the remote one.
    '''
    git_init(ctx)
    # host_name = ctx.run('hostname').stdout.strip()

    # do not prompt for ssl certificate passphrase
    ctx.local('git config http.sslVerify false')
    ctx.local('git config http.postBuffer 524288000')

    # remove the remote server link
    ctx.local('git remote remove {}'.format(ctx.original_host), warn=True)

    # add remote back
    ctx.local('git remote add {} ssh://{}@{}:{}'.format(
        ctx.original_host,
        ctx.user,
        ctx.original_host,
        repo_path(ctx))
    )
    ctx.local('git push --set-upstream {} master'.format(
        ctx.original_host), replace_env=False)


@task
def git_push(ctx):
    ''' sync before push
    '''
    branch = ctx.config.repo.branch
    # host_name = ctx.run('hostname').stdout.strip()
    ctx.local('git push --set-upstream {} {}'.format(
        ctx.original_host,
        branch), replace_env=False)


@task
def git_checkout(ctx):
    '''clone or fetch the git repo on remote host

    must use repo.sync before this task
    '''
    webapp = Path(ctx.config.www.root, ctx.config.www.project).resolve()
    path = repo_path(ctx)
    branch = ctx.config.repo.branch
    with ctx.cd(str(webapp)):
        if not exists(ctx, '.git'):
            ctx.run('git clone {} .'.format(path))
            ctx.run('git checkout {}'.format(branch))
        else:
            ctx.run('git fetch')
            ctx.run('git checkout --force {}'.format(branch))
            ctx.run('git pull origin')


ns = module_namespace(__name__)
