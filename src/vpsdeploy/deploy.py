from pathlib import Path
from fabric import task
from .util import task_namespace
from .server import upgrade as server_upgrade, install as server_install
from .www import webapp
from .repo import git_sync, git_push, git_checkout
from .service import service_setup, service_enable, service_status
from .nginx import reload_nginx
from .venv import venv_install


@task
def web_setup(ctx):
    '''setup web app on server
    '''
    webapp(ctx)
    git_sync(ctx)
    git_push(ctx)    # git push to server
    git_checkout(ctx)  # git checkout
    venv_install(ctx)  # python venv
    service_setup(ctx)  # create uwsgi, nginx and systemd unit files.
    service_enable(ctx)
    service_status(ctx)
    reload_nginx(ctx)


@task
def refresh(ctx):
    '''refresh the web app files from git repo
    '''
    git_push(ctx)
    git_checkout(ctx)
    venv_install(ctx)


ns = task_namespace(__name__, web_setup, refresh)
