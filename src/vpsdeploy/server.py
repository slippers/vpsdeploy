import re
from fabric import task
from invoke.watchers import Responder
from .util import module_namespace
from .keyring import sudo_password


@task
def uname(ctx):
    '''uname -a
    '''
    ctx.run('uname -a')


@task
def update(ctx):
    '''apt-get update
    '''
    sudo_password(ctx)
    ctx.sudo('apt-get update')


@task
def upgrade(ctx):
    '''apt-get upgrade
    '''
    sudo_password(ctx)
    update(ctx)
    responder = Responder(
        pattern=re.escape("Do you want to continue? [Y/n]"),
        response='y\n'
    )
    ctx.sudo('apt-get upgrade', watchers=[responder])


@task
def install(ctx):
    '''apt-get install nginx and uwsig packages decalared in config.install
    '''
    sudo_password(ctx)
    ctx.sudo('apt-get install {}'.format(ctx.config.install.nginx))
    ctx.sudo('apt-get install {}'.format(ctx.config.install.uwsgi))


ns = module_namespace(__name__)
