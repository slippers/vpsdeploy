from pathlib import Path
from string import Template


def build_service_makefile(ctx):
    template = """.DEFAULT_GOAL := all
SYSTEMD_HOME := /etc/systemd/system
SOCKET_DIR := $socket_dir
INITCTL:=$(shell which initctl)
SYSTEMCTL:=$(shell which systemctl)
UWSGI:=$(shell which uwsgi)

.PHONY: all status install uninstall enable disable nginx

all:
\t@echo "use enable and disable"
\t@echo uwsgi=$(UWSGI)
\tuwsgi --version
\t@echo UWSGI_INI=$(UWSGI_INI)
\t@echo SOCKET_DIR=$(SOCKET_DIR)

status: all
\t@echo sudo journalctl -xe -n 10 -u uwsgi-app@*.service
\tsystemctl --all list-sockets 'uwsgi-app@*'
\tsystemctl --all list-unit-files 'uwsgi-app@*'
\tsystemctl --all list-units 'uwsgi-app@*'
\tsystemctl status 'uwsgi-app@*'

install:
\tinstall -m 0644 -t $(SYSTEMD_HOME) uwsgi-app@.socket uwsgi-app@.service uwsgi-app.service

uninstall: disable
\trm -f $(SYSTEMD_HOME)/uwsgi-app.service
\trm -f $(SYSTEMD_HOME)/uwsgi-app@.socket
\trm -f $(SYSTEMD_HOME)/uwsgi-app@.service

disable:
\tsystemctl stop uwsgi-app.service
\tsystemctl disable uwsgi-app.service
\tsystemctl reset-failed uwsgi-app@*.socket
\tsystemctl stop uwsgi-app@*.socket
\tsystemctl disable uwsgi-app@*.socket
\tsystemctl stop uwsgi-app@*.service
\tsystemctl disable uwsgi-app@*.service
\tsystemctl daemon-reload
"""
    result = Template(template).safe_substitute(
        project=ctx.config.www.project,
        project_root=ctx.config.www.root,
        socket_dir=ctx.config.uwsgi.socket,
    )
    return (result, 'Makefile')



def build_makefile(ctx):
    template = """.DEFAULT_GOAL := all
SYSTEMD_HOME := /etc/systemd/system
PROJECT := $project
PROJECT_ROOT := $project_root/$(PROJECT)
UWSGI_INI := $(PROJECT).ini
SOCKET_DIR := $socket_dir

INITCTL:=$(shell which initctl)
SYSTEMCTL:=$(shell which systemctl)
UWSGI:=$(shell which uwsgi)

.PHONY: all status install uninstall enable disable nginx

all:
\t@echo "use enable and disable"
\t@echo uwsgi=$(UWSGI)
\tuwsgi --version
\t@echo PROJECT=$(PROJECT)
\t@echo PROJECT_ROOT=$(PROJECT_ROOT)
\t@echo UWSGI_INI=$(UWSGI_INI)
\t@echo SOCKET_DIR=$(SOCKET_DIR)

status: all
\t@echo sudo journalctl -xe -n 10 -u uwsgi-app@$(PROJECT).service
\tsystemctl --all list-sockets 'uwsgi-app@$(PROJECT)*'
\tsystemctl --all list-unit-files 'uwsgi-app@$(PROJECT)*'
\tsystemctl --all list-units 'uwsgi-app@$(PROJECT)*'
\tsystemctl status 'uwsgi-app@$(PROJECT)*'

enable:
\tsystemctl enable uwsgi-app@$(PROJECT).socket
\tsystemctl enable uwsgi-app@$(PROJECT).service
\tsystemctl start uwsgi-app@$(PROJECT).socket

disable:
\tsystemctl reset-failed uwsgi-app@$(PROJECT).socket
\tsystemctl stop uwsgi-app@$(PROJECT).socket
\tsystemctl disable uwsgi-app@$(PROJECT).socket
\tsystemctl stop uwsgi-app@$(PROJECT).service
\tsystemctl disable uwsgi-app@$(PROJECT).service
\tsystemctl daemon-reload
"""
    result = Template(template).safe_substitute(
        project=ctx.config.www.project,
        project_root=ctx.config.www.root,
        socket_dir=ctx.config.uwsgi.socket,
    )
    return (result, 'Makefile')


def build_celery_makefile(ctx, c):
    template = """.DEFAULT_GOAL := all
SYSTEMD_HOME := /etc/systemd/system
PROJECT := $project
WEBAPP := $webapp
CELERY_CONF := $config_name
CELERY_SERVICE := $service_name
RUN_DIR := $run_dir
LOG_DIR := $log_dir

INITCTL:=$(shell which initctl)
SYSTEMCTL:=$(shell which systemctl)
CELERY:=$(shell which celery)

.PHONY: all status install uninstall enable disable

all:
\t@echo "use enable and disable"
\t@echo PROJECT=$(PROJECT)
\t@echo WEBAPP=$(WEBAPP)
\t@echo CELERY_CONF=$(CELERY_CONF)
\t@echo RUN_DIR=$(RUN_DIR)
\t@echo LOG_DIR=$(LOG_DIR)

status: all
\t@echo sudo journalctl -xe -n 10 -u $(CELERY_SERVICE)
\tsystemctl status $(CELERY_SERVICE)

install:
\tinstall -m 0644 -t $(SYSTEMD_HOME) $(CELERY_SERVICE)

enable: install
\tsystemctl enable $(CELERY_SERVICE)
\tsystemctl start $(CELERY_SERVICE)

disable:
\tsystemctl stop $(CELERY_SERVICE)
\tsystemctl disable $(CELERY_SERVICE)

uninstall: disable
\tsystemctl reset-failed $(CELERY_SERVICE)
\tsystemctl stop $(CELERY_SERVICE)
\trm -f $(SYSTEMD_HOME)/$(CELERY_SERVICE)
\tsystemctl daemon-reload
"""
    result = Template(template).safe_substitute(
        project=c.project,
        webapp=c.webapp,
        config_name=c.config_name,
        service_name=c.service_name,
        run_dir=c.run_dir,
        log_dir=c.log_dir,
    )
    return (result, 'Makefile')


