from pathlib import Path
from pkg_resources import (
    resource_listdir,
    resource_isdir,
    get_default_cache,
    cleanup_resources,
    resource_filename,
)



def resource_prepare():
    print(get_default_cache())
    print(cleanup_resources())

def resource_walk(package_or_requirement, resource_name):
    queue = [resource_name]
    while len(queue) > 0:
        dirpath = queue.pop()
        dirnames = []
        filenames = []
        for name in resource_listdir(package_or_requirement, dirpath):
            fullpath = os.path.join(dirpath, name)
            if resource_isdir(package_or_requirement, fullpath):
                dirnames.append(name)
                queue.append(fullpath)
            else:
                filenames.append(name)
        yield dirpath, dirnames, filenames


def resource_extract(package_or_requirement, dirpath, dirnames, filenames):
    for file in filenames:
        file_path = Path(dirpath, file)
        print(resource_filename(package_or_requirement, str(file_path)))


