import pprint
from fabric import task
from .util import module_namespace, load_config


@task
def default(ctx):
    '''print the local project fabric config settings
    '''
    pprint.pprint(load_config())


@task
def context(ctx):
    '''report on invoke context configuration

    Sometimes you need to see what the config
    is.  Config files from local to home could be used.
    '''
    for key in load_config():
        print(key)
        pprint.pprint(ctx.config[key])


ns = module_namespace(__name__)
