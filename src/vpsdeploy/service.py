from pathlib import Path
from fabric import task
from patchwork.files import directory, exists
from .util import (
    module_namespace,
    print_parser,
    save_local,
    save_local_file,
)
from .uwsgi import build_uwsgi_ini
from .systemd import (
    build_systemd_service,
    build_systemd_socket,
    build_systemd_socket_service,
)
from .makefile import build_makefile, build_service_makefile
from .nginx import build_nginx_config
from .keyring import sudo_password


@task
def service_setup(ctx):
    '''send the files to the remote host webapp
    they can then be activated there.
    '''
    setup_vpsdeploy(ctx)
    setup_uwsgi(ctx)
    service_enable(ctx)


@task
def setup_uwsgi(ctx):
    uwsgi_parser, uwsgi_name  = build_uwsgi_ini(ctx)
    uwsgi_file = save_local(uwsgi_parser, uwsgi_name, 'uwsgi')

    makefile_text, makefile_name = build_makefile(ctx)
    makefile = save_local_file(makefile_text, makefile_name, 'uwsgi')

    nginx_text, nginx_name  = build_nginx_config(ctx);
    nginx_config = save_local_file(nginx_text, nginx_name, 'uwsgi')

    webapp = Path(ctx.config.www.root, ctx.config.www.project)
    uwsgi_dir = Path(webapp, 'uwsgi')
    directory(ctx, uwsgi_dir)

    ctx.put(uwsgi_file, str(uwsgi_dir))
    ctx.put(makefile, str(uwsgi_dir))
    ctx.put(nginx_config, str(uwsgi_dir))


@task
def setup_vpsdeploy(ctx):
    '''create vpsdeploy in the home directory. populate the make and service files.
    '''

    vpsdeploy_home = ctx.config.service.home

    socket_parser, socket_name = build_systemd_socket(ctx)
    socket_file = save_local(socket_parser, socket_name, vpsdeploy_home)

    service_parser, service_name = build_systemd_service(ctx)
    service_file = save_local(service_parser, service_name, vpsdeploy_home)

    socket_service_parser, socket_service_name = build_systemd_socket_service(ctx)
    socket_service_file = save_local(socket_service_parser, socket_service_name, vpsdeploy_home)

    makefile_text, makefile_name = build_service_makefile(ctx)
    makefile = save_local_file(makefile_text, makefile_name, vpsdeploy_home)

    directory(ctx, vpsdeploy_home)

    ctx.put(socket_file, str(vpsdeploy_home))
    ctx.put(socket_service_file, str(vpsdeploy_home))
    ctx.put(service_file, str(vpsdeploy_home))
    ctx.put(makefile, str(vpsdeploy_home))


@task
def service_enable(ctx):
    '''execute the make enable on the remote host
    '''
    sudo_password(ctx)

    vpsdeploy_home = ctx.config.service.home
    ctx.sudo('make install --directory {}'.format(vpsdeploy_home))

    webapp = Path(ctx.config.www.root, ctx.config.www.project)
    uwsgi_dir = Path(webapp, 'uwsgi')
    ctx.sudo('make enable --directory {}'.format(uwsgi_dir))


@task
def service_status(ctx):
    '''execute the make status on the remote host
    '''
    sudo_password(ctx)

    vpsdeploy_home = ctx.config.service.home
    ctx.sudo('make status --directory {}'.format(vpsdeploy_home))

    webapp = Path(ctx.config.www.root, ctx.config.www.project)
    uwsgi_dir = Path(webapp, 'uwsgi')
    ctx.sudo('make status --directory {}'.format(uwsgi_dir))


ns = module_namespace(__name__)
