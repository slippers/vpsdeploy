from pathlib import Path
from fabric import task
from patchwork.files import directory, exists
from .nginx import ssl_config
from .util import module_namespace, save_local_file
from .keyring import sudo_password


'''http validation of a provider ssl certificate is necessary
when you don't have a configured domain email address.  the provider
will supply you with a validation file that will need to be placed in
a specific url.  one made accessable from your server you can complete
the validation process.
'''


@task
def http_validation(ctx):
    '''construct a nginx web directory. configure a nginx.conf file
    and subfolder to store validation file.  after constrution you can
    use the include statement in your nginx server node your are
    configuring for ssl.
    '''
    sudo_password(ctx)
    path = Path(ctx.config.www.root, ctx.config.ssl.http_validation).resolve()
    files = path / 'files'

    if not exists(ctx, path):
        directory(ctx,
                  path,
                  user=ctx.config.ssl.user,
                  group=ctx.config.ssl.group,
                  mode=ctx.config.ssl.mode,
                  sudo=True)

        directory(ctx, files,
                  user=ctx.config.ssl.user,
                  group=ctx.config.ssl.group,
                  mode=ctx.config.ssl.mode,
                  sudo=True)

    nginx_text, nginx_name = ssl_config(ctx)
    nginx_config = save_local_file(
        nginx_text,
        nginx_name,
        ctx.config.ssl.http_validation)
    ctx.put(nginx_config, str(path))

    print('include', str(path / nginx_name) + ';')

    test = save_local_file('test', 'test.txt', 'ssl')
    ctx.put(test, str(files))


@task
def upload_validation(ctx):
    '''upload all txt files found in http_validation directory to the remote web directory
    config.ssl.http_validation
    '''
    path = Path(ctx.config.www.root, ctx.config.ssl.http_validation, 'files').resolve()
    if not exists(ctx, path):
        print('you must first build the remote web directory using http_validation task.')
        return

    print('preparing to upload to:', path)

    validation = Path(Path.cwd(), ctx.config.ssl.http_validation)
    txt_paths = [pth for pth in validation.iterdir() if pth.suffix == '.txt']

    if len(txt_paths) == 0:
        print('missing txt files to upload:', validation)
        return

    for txt in txt_paths:
        print('uploading:', txt)
        ctx.put(txt, str(path))


ns = module_namespace(__name__)
