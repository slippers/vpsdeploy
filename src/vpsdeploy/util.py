import sys
import pprint
from pathlib import Path
from invoke import Collection, Task
import yaml


def module_namespace(name):
    '''generate a invoke Collection with config

    usage:
        # place at the end of the module to be loaded last.
        config = {'setting': 'value'}
        ns = module_namespace(__name__, config)
    '''
    ycfg = load_config()
    ns = Collection.from_module(sys.modules[name])
    ns.configure(ycfg)
    return ns

def task_namespace(module, *tasks):
    module_name = module.split(".")[1]
    ycfg = load_config()
    ns = Collection(module_name)
    ns.configure(ycfg)
    for task in tasks:
        ns.add_task(task)
    return ns


def load_config():
    here = Path(__file__).parent
    with (here / 'config.yaml').open() as cfg:
        return yaml.safe_load(cfg)


def dump_config():
    config = load_config()
    return yaml.safe_dump(config)


def save_local_file(text, name, directory='.'):
    path = Path(Path.cwd(), directory)
    path.mkdir(exist_ok=True)
    file = path / name
    file.write_text(text)
    return file


def save_local(parser, name, directory='.'):
    path = Path(Path.cwd(), directory)
    path.mkdir(exist_ok=True)
    file = path / name
    with file.open(mode='w') as f:
        parser.write(f)
    return file


def print_parser(parser):
    pprint.pprint({section: dict(parser.items(section)) for section in parser.sections()})


def www_user_name(ctx):
    user = ctx.config.www.user
    if user:
        return user

    if ctx.config.www.project:
        user = 'www-{}'.format(ctx.config.www.project)
        return user

    return None
