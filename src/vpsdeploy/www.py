from pathlib import Path
from fabric import task
from invoke import AmbiguousEnvVar
from patchwork.files import directory, exists
from .util import module_namespace, www_user_name
from .keyring import sudo_password

"""
Update: the following seems to have worked:

    Establish a [new directory] at /var/www

    Change the directory owner and group:
    sudo chown www-data:www-data /var/www/[new directory]

    allow the group to write to the directory with appropriate permissions:
    sudo chmod -R 775 /var/www

    Add myself to the www-data group:
    sudo usermod -a -G www-data [my username]

"""


@task
def www_data(ctx):
    '''connected user is in the www-data group
    '''
    sudo_password(ctx)

    if ctx.run('id -Gn {} | grep -c {}'.format(
        ctx.user,
        ctx.config.www.group), warn=True
    ).ok:
        print('user {} is in group {}', ctx.user, ctx.config.www.group)
        return

    ctx.sudo('usermod -a -G {} {}'.format(
        ctx.config.www.group,
        ctx.user), echo=True
    )

    print('user {} added group {}', ctx.user, ctx.config.www.group)


@task
def www_user(ctx):
    '''create system linux user account
    useradd with no home or login
    usermod -L to lock the password
    '''
    sudo_password(ctx)
    www_data(ctx)
    user = www_user_name(ctx)

    if ctx.run('id -u {}'.format(user), warn=True).ok:
        print('user {} already exits.'.format(user))
    else:
        useradd = 'useradd --system \
                --no-create-home \
                --groups {} {}'.format(ctx.config.www.group, user)

        ctx.sudo(useradd, echo=True)

        ctx.sudo('usermod -L {}'.format(user), echo=True)

    return user


@task
def webapp(ctx):
    '''create webapp directory on remote host
    needs sudo
    '''
    sudo_password(ctx)
    user = www_user(ctx)
    path = Path(ctx.config.www.root, ctx.config.www.project).resolve()

    if not exists(ctx, path):
        directory(ctx,
                  path,
                  user=user,
                  group=ctx.config.www.group,
                  mode=ctx.config.www.mode,
                  sudo=True)

    ctx.run('stat -c "%A %a %n" {}'.format(path))


ns = module_namespace(__name__)
