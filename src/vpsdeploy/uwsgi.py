import os
from pathlib import Path
from string import Template
from configparser import ConfigParser
from .util import www_user_name

'''
https://uwsgi-docs.readthedocs.io/en/latest/Options.html
logging 
python application mounting

virtualenv

process security
'''

def socket_path(ctx):
    return Path(ctx.config.uwsgi.socket, ctx.config.www.project + '.socket')


def build_uwsgi_ini(ctx):
    project = ctx.config.www.project
    webapp = Path(ctx.config.www.root, ctx.config.www.project)
    application = ctx.config.uwsgi.application
    venv = Path(webapp, ctx.config.venv.source)
    git_index = Path(webapp, '.git/index')
    logger = 'file:{}'.format(Path(ctx.config.uwsgi.logger, project, 'error.log'))
    req_logger = 'file:{}'.format(Path(ctx.config.uwsgi.logger, project, 'access.log'))
    socket = socket_path(ctx)
    user = www_user_name(ctx)
    group = ctx.config.www.group

    template = { 'uwsgi': {
        'chdir':        webapp,
        'virtualenv':   venv,
        'manage-script-name': True,
        'mount':        '/{}={}'.format(project, application),
        'master':       True,
        'processes':    5,
        'vacuum':       True,
        'die-on-term':  True,
        'die-on-idle':  True,
        'plugins':      'logfile, python3',
        'log-date':     '%%Y-%%m-%%d %%H:%%M:%%S',
        'req-logger':   req_logger,
        'logger':       logger,
        'socket':       socket,
        'chown-socket': '{}:{}'.format(user, group),
        'chmod-socket': 666,
        'uid':          user,
        'gid':          group,
        'touch-reload': git_index,
#        'lazy-apps':    True,
        }
    }

    if ctx.config.uwsgi.env:
        env = webapp / ctx.config.uwsgi.env
        template['uwsgi']['for-readline'] = env.resolve()
        template['uwsgi']['  env'] = '%(_)'
        template['uwsgi']['end-for'] = ''

    parser = ConfigParser(interpolation=None)
    parser.optionxform=str
    parser.read_dict(template)
    return (parser, 'uwsgi.ini')
