from pathlib import Path
import crossplane
from patchwork.files import directory, exists
from fabric import task
from .util import module_namespace
from .uwsgi import socket_path
from .keyring import sudo_password


'''
https://github.com/nginxinc/crossplane
'''


def build_nginx_config(ctx):
    '''
    generate nginx.conf for the site.
    append one or more static locations
    '''
    socket = "unix://{}".format(socket_path(ctx))
    app = ctx.config.nginx.app
    payload = [
        {
            "directive": "location",
            "args": [app],
            "block": [
                {
                    "directive": "include",
                    "args": ["uwsgi_params"]
                },
                {
                    "directive": "uwsgi_pass",
                    "args": [socket]
                },
            ]
        },
    ]

    for location in ctx.config.nginx.locations:
        location_path = Path(ctx.config.nginx.app, location)
        location_root = Path(
            ctx.config.www.root,
            ctx.config.www.project,
            location)

        print(location_path, location_root)
        if not exists(ctx, location_root):
            directory(ctx, location_root)

        alias = {
            "directive": "location",
            "args": [str(location_path)],
            "block": [
                {
                    "directive": "alias",
                    "args": [str(location_root)]
                },
            ]
        }
        payload.append(alias)

    built = crossplane.build(payload, indent=4, tabs=False)
    return (built, 'nginx.conf')


def ssl_config(ctx):
    '''
    location ^~ /.well-known/pki-validation/ {
        default_type "text/plain";
        alias /var/www/pki-validation/files/;
    }
    '''

    path = str(Path(ctx.config.www.root, ctx.config.ssl.http_validation, 'files')) + '/'
    payload = [
        {
            "directive": "location",
            "args": ["^~", ctx.config.ssl.http_validation_url],
            "block": [
                {
                    "directive": "default_type",
                    "args": ["text/plain"]
                },
                {
                    "directive": "alias",
                    "args": [path]
                }
            ]
        }
    ]
    built = crossplane.build(payload, indent=4, tabs=False)
    return (built, 'nginx.conf')


@task
def reload_nginx(ctx):
    sudo_password(ctx)
    ctx.sudo('systemctl reload nginx.service')
    ctx.sudo('nginx -t')


ns = module_namespace(__name__)
