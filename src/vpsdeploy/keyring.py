import keyring


def sudo_password(ctx):
    '''use cli command `vps_deploy set-host-key` to create key
    '''
    ctx.config.sudo.password = keyring.get_password(ctx.host, ctx.user)
    if not ctx.config.sudo.password:
        raise Exception('sudo password not set in keyring:{} {}'.format(
            ctx.host, ctx.user)
        )
