import pprint
'''Container for attributes

Container.yourattribute = value
'''
class Container():
    @staticmethod
    def attributes():
        attr = {}
        for i in dir(Container):
            if not i.startswith('__') :
                attr[i] = getattr(Container,i)

        return attr


    @staticmethod
    def display():
        return pprint.pprint(Container.attributes())
