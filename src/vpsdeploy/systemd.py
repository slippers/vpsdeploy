from pathlib import Path
from configparser import ConfigParser
from .util import www_user_name

'''
https://uwsgi-docs.readthedocs.io/en/latest/Systemd.html

these are genertic systemd unit files that will
start any number of configured uwsgi process.

create directory /var/log/uwsgi/project for the uwsgi process.

'''
def build_systemd_service(ctx):
    '''this is a parent service that creates the /var/run/uwsgi directory
    # install to: /etc/systemd/system
    # uwsgi-app.service
    '''
    uwsgi_socket_dir = Path(ctx.config.uwsgi.socket)
    template = {
        'Unit': {
            'Description':'uwsgi-app service',
            'After': 'network.target',
        },
        'Service': {
            'Type':'oneshot',
            'ExecStart':'/bin/true',
            'ExecReload':'/bin/true',
            'RemainAfterExit':'on',
            'User': ctx.config.www.group,
            'Group': ctx.config.www.group,
            'RuntimeDirectory': 'uwsgi',
            'RuntimeDirectoryMode': '0775',
        },
        'Install': {
            'WantedBy': 'multi-user.target',
        }
    }
    parser = ConfigParser(interpolation=None)
    parser.optionxform=str
    parser.read_dict(template)
    return (parser, 'uwsgi-app.service')


def build_systemd_socket(ctx):
    '''socket service connects with the webserver application via socket files.
    socket will start the corresponding service for the app.
    # install to: /etc/systemd/system
    # uwsgi-app@.socket
    '''
    uwsgi_socket_dir = Path(ctx.config.uwsgi.socket)
    template = {
        'Unit': {
            'Description':'Socket for uWSGI app %i',
            'Requires': 'uwsgi-app.service',
        },
        'Socket': {
            'ListenStream': uwsgi_socket_dir / '%i.socket',
            'SocketUser': 'www-%i',
            'SocketGroup': ctx.config.www.group,
            'SocketMode': '0660',
        },
        'Install': {
            'WantedBy': 'sockets.target',
        }
    }
    parser = ConfigParser(interpolation=None)
    parser.optionxform=str
    parser.read_dict(template)
    return (parser, 'uwsgi-app@.socket')


def build_systemd_socket_service(ctx):
    '''
    a service activated by the corresponding socket service.
    does the actual work of hosting the uwsgi app.
    # install to: /etc/systemd/system
    # uwsgi-app@.service
    '''
    www_root = ctx.config.www.root
    project = ctx.config.www.project
    binary = ctx.config.uwsgi.binary
    user = www_user_name(ctx)
    group = ctx.config.www.group
    template = {
        'Unit': {
            'Description':'%i uWSGI app',
            'After': 'syslog.target',
        },
        'Service': {
            'User':             'www-%i',
            'Group':            group,
            'TimeoutStartSec':  0,
            'RestartSec':       10,
            'Restart':          'on-failure',
            'KillSignal':       'SIGQUIT',
            'Type':             'notify',
            'StandardError':    'syslog',
            'NotifyAccess':     'all',
            'LogsDirectory':    'uwsgi/%i',
            'ExecStart': '{} --ini {}/%i/uwsgi/uwsgi.ini'.format(binary, www_root),
        },
        'Install': {
            'WantedBy': 'multi-user.target'
        }
    }
    parser = ConfigParser(interpolation=None, allow_no_value=True)
    parser.optionxform=str
    parser.read_dict(template)
    return (parser, 'uwsgi-app@.service')


def build_systemd_celery_service(ctx, c):

    start = '${CELERY_BIN} multi start ${CELERYD_NODES} \
-A ${CELERY_APP} \
--pidfile=${CELERYD_PID_FILE} \
--logfile=${CELERYD_LOG_FILE} \
--loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'

    stop =  '${CELERY_BIN} multi stopwait ${CELERYD_NODES} \
--pidfile=${CELERYD_PID_FILE}'

    reload = '${CELERY_BIN} multi restart ${CELERYD_NODES} \
-A ${CELERY_APP} \
--pidfile=${CELERYD_PID_FILE} \
--logfile=${CELERYD_LOG_FILE} \
--loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'

    template = {
        'Unit': {
            'Description':'celery app {}'.format(c.project),
            'After': 'network.target'
        },
        'Service': {
            'Type': 'forking',
            'User': c.user,
            'Group': c.group,
            'EnvironmentFile': c.config,
            'WorkingDirectory': c.webapp,
            'LogsDirectory': 'celery/{}'.format(c.project),
            'RuntimeDirectory':  'celery/{}'.format(c.project),
            'RuntimeDirectoryMode': '0750',
            'PIDFile': c.pid_file,
            'ExecStart': '/bin/sh -c \'{}\''.format(start),
            'ExecStop':'/bin/sh -c \'{}\''.format(stop),
            'ExecReload':'/bin/sh -c \'{}\''.format(reload),
        },
        'Install': {
            'WantedBy': 'multi-user.target',
        }
    }
    parser = ConfigParser(interpolation=None)
    parser.optionxform=str
    parser.read_dict(template)
    return parser

