from pathlib import Path
from configparser import ConfigParser
from fabric import task
from patchwork.files import directory, exists
from configparser import ConfigParser
from string import Template
from .systemd import build_systemd_celery_service
from .makefile import build_celery_makefile
from .util import (
    module_namespace,
    save_local_file,
    save_local,
    www_user_name,
)
from .container import Container


def config_extract(ctx):
    Container.celery = 'celery'
    Container.project = ctx.config.www.project
    Container.webapp = Path(ctx.config.www.root, ctx.config.www.project)
    Container.user = www_user_name(ctx)
    Container.group = ctx.config.www.group
    Container.mode = ctx.config.celery.mode
    Container.celery_dir = Path(ctx.config.www.root, ctx.config.www.project, Container.celery)
    Container.config_name = ctx.config.celery.config_name
    Container.config = Container.celery_dir / Container.config_name
    Container.venv = Path(Container.webapp, ctx.config.venv.source)
    Container.celery_bin = Container.venv / ctx.config.celery.celery_bin
    Container.app = ctx.config.celery.app
    Container.run_dir = Path(ctx.config.celery.run_dir, Container.project)
    Container.pid_file = Path(Container.run_dir, Container.project + '.pid')
    Container.log_dir = Path(ctx.config.celery.log_dir, Container.project)
    Container.log_file = Container.log_dir / '%n%I.log'
    Container.service_name = 'celery-{}.service'.format(Container.project)

    return Container


@task
def setup(ctx):
    '''setup celery
    '''
    c = config_extract(ctx)

    c.display()

    config_parser = build_config_ini(ctx, c)
    config_file = save_local(config_parser, c.config_name, Container.celery)

    service_parser = build_systemd_celery_service(ctx, c)
    service_file = save_local(service_parser, c.service_name, Container.celery)

    makefile_text, makefile_name = build_celery_makefile(ctx, c)
    makefile = save_local_file(makefile_text, makefile_name, Container.celery)

    if not exists(ctx, c.celery_dir):
        directory(ctx,
                  c.celery_dir,
                  user=c.user,
                  group=c.group,
                  mode=c.mode,
                  sudo=True)

    ctx.put(config_file, str(c.celery_dir))
    ctx.put(service_file, str(c.celery_dir))
    ctx.put(makefile, str(c.celery_dir))


def build_config_ini(ctx, c):
    '''
    build config file for celery process
    '''
    template = {
        'DEFAULT': {
            'CELERYD_NODES': c.project,
            'CELERY_BIN': c.celery_bin,
            'CELERY_APP': c.app,
            'CELERYD_MULTI':"multi",
            'CELERYD_OPTS':"--time-limit=1800 --concurrency=8",
            'CELERYD_PID_FILE': c.pid_file,
            'CELERYD_LOG_FILE': c.log_file,
            'CELERYD_LOG_LEVEL':"INFO",
            'CELERYD_USER': c.user,
            'CELERYD_GROUP': c.group,
            'CELERY_CREATE_DIRS': 1
        }
    }
    parser = ConfigParser(interpolation=None)
    parser.optionxform=str
    parser.read_dict(template)
    return parser


ns = module_namespace(__name__)
