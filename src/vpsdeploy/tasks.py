from invoke import Collection
from .repo import ns as repo_ns
from .settings import ns as settings_ns
from .www import ns as www_ns
from .venv import ns as venv_ns
from .service import ns as service_ns
from .server import ns as server_ns
from .deploy import ns as deploy_ns
from .ssl import ns as ssl_ns
from .celery import ns as celery_ns

ns = Collection()
ns.add_collection(repo_ns)
ns.add_collection(settings_ns)
ns.add_collection(www_ns)
ns.add_collection(venv_ns)
ns.add_collection(service_ns)
ns.add_collection(server_ns)
ns.add_collection(deploy_ns)
ns.add_collection(ssl_ns)
ns.add_collection(celery_ns)
