from pathlib import Path
from fabric import task
from patchwork.files import directory, exists
from .util import module_namespace


@task
def venv_setup(ctx):
    '''setup and activate python3 virtualenv
    uses:
        config.www.root
        config.www.project
        config.venv.create
        config.venv.source
    '''
    webapp = Path(ctx.config.www.root, ctx.config.www.project).resolve()
    with ctx.cd(str(webapp)):
        if not exists(ctx, ctx.config.venv.source):
            ctx.run(ctx.config.venv.create)
            with ctx.prefix('source {}/bin/activate'.format(
                ctx.config.venv.source)
            ):
                ctx.run('pip -V')
                ctx.run('pip install wheel')


@task
def venv_install(ctx):
    '''install python requrirments using `pip install`
    uses:
        config.venv.requirements
        config.venv.source
        config.www.root
        config.www.project
    '''
    venv_setup(ctx)
    webapp = Path(ctx.config.www.root, ctx.config.www.project).resolve()
    with ctx.cd(str(webapp)):
        with ctx.prefix('source {}/bin/activate'.format(ctx.config.venv.source)):
            ctx.run('pip install --upgrade -r {}'.format(ctx.config.venv.requirements))


ns = module_namespace(__name__)
