#!/usr/bin python

import click
from pathlib import Path
import shutil
import yaml
import subprocess
import invoke
import keyring
from vpsdeploy.util import dump_config


fabfile = Path('fabric.yaml')


@click.group()
def main():
    """welcome to vps_deploy.

    example:

    fab -c vpsdeploy -H user@host -f fabric.yaml --list
    """


@main.command()
@click.argument('host')
@click.argument('username')
@click.option('--password', prompt=True,
              hide_input=True,
              confirmation_prompt=True)
@click.pass_obj
def set_host_key(ctx, host, username, password):
    '''add the sudo username and password for a host
    keyring python package exposes the gnome keyring system
    look for it under 'Passwords and Keys'
    '''
    click.echo('setting key in keyring: {}'.format(keyring.get_keyring()))
    keyring.set_password(host, username, password)
    if keyring.get_password(host, username):
        click.echo('key created.')


@main.command()
def list():
    '''list all the vpsdeploy commands.
    '''
    click.echo('list of fab tasks')
    command = 'fab -f fabric.yaml -c vpsdeploy -l'
    click.echo(command)
    subprocess.run(command.split())


@main.command()
def config_display():
    '''show your current fabric.yaml config
    '''
    click.echo('configuration of vpsdeploy.')
    click.echo(dump_config())

    if not fabfile.exists():
        click.echo('local fabric.yaml not found.')
        return

    click.echo('local fabric.yaml found.')
    with fabfile.open() as f:
        y = yaml.safe_load(f)
        click.echo(yaml.safe_dump(y))


@main.command()
def config_build():
    '''generate the default fabric.yaml config file.
    '''
    if fabfile.exists():
        click.echo('Found fabric.yaml, remove to continue.')
        return

    fabfile.write_text(dump_config())


@main.command()
def invoke_patch():
    '''patch the invoke.loader.py file to include the system paths.

    parents.extend(os.sys.path)
    '''
    loader = Path(invoke.loader.__file__)
    loader_bak = Path(loader.parent, '{}.bak'.format(loader.name))
    loader_orig = Path(Path(__file__).parent, 'loader.py.orig')
    loader_patch = Path('loader.py.patch')

    click.echo('unpatch_invoke will restore invoke.loader.py')

    # if backup exists overwrite the loader.py with ~loader.py
    if not loader_bak.exists():
        shutil.copy(str(loader), str(loader_bak))

    # create a patch file from the diff tool.
    with open(loader_patch, 'w') as patch_file:
        diff = subprocess.run([
            "diff",
            "--context",
            str(loader),
            str(loader_orig)
        ],
            stdout=patch_file,
            stderr=subprocess.PIPE)

    if diff.returncode == 0:
        click.echo('invoke.loader.py already patched.')
        return

    if diff.returncode == 2:
        click.echo('diff tool found trouble.')
        click.echo(diff.stdout)
        click.echo(diff.stderr)
        return

    if diff.returncode == 1:
        click.echo('invoke.loader.py needs to be changed.')
        click.echo(diff.stdout)

    # patch the loader.py using the patch file created
    command = [
        "patch",
        "-d",
        str(loader.parent),
        "-i",
        str(loader_patch.absolute())
    ]
    print(' '.join(command))
    patch = subprocess.run(command,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)

    if patch.returncode == 0:
        click.echo('invoke.loader.py has been patched.')

    print(patch.stdout, patch.stderr)


@main.command()
def invoke_unpatch():
    '''restore the patched invoke.loader.py file
    '''
    loader = Path(invoke.loader.__file__)
    loader_bak = Path(loader.parent, '{}.bak'.format(loader.name))

    # if backup exists overwrite the loader.py with ~loader.py
    if loader_bak.exists():
        click.echo('restoring invoke.loader.py.')
        shutil.copy(str(loader_bak), str(loader))
        loader_bak.unlink()


if __name__ == "__main__":
    main()
